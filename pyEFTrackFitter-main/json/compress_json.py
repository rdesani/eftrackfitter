#!/usr/bin/env python

"""
Helper script to 'compress' a JSON file by eliminating spurious whitespace
and newlines. For human readability we add these.
"""

import argparse
import json
import os
import sys

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="Input JSON file to run over.")
    parser.add_argument("-i", "--inplace", action="store_true", dest="inplace", help="Overwrite input file in place.")
    args = parser.parse_args()

    path = os.path.abspath(os.path.expanduser(args.infile))
    if not os.path.exists(path):
        print("Error: no such file or directory: " + path)
        sys.exit(1)

    with open(path, 'r') as jsonfile:
        text = json.load(jsonfile)

    if args.inplace:
        output_path = path
    else:
        output_dir = os.path.dirname(args.infile)
        output_file = "compress_" + os.path.basename(path)
        output_path = os.path.join(output_dir, output_file)

    with open(output_path, 'w') as jsonfile:
        json.dump(text, jsonfile, indent=None)

if __name__ == '__main__':
    main()
