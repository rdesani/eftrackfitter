#!/usr/bin/env python

# This script is NOT ULTIMATELY WHAT WE WANT TO USE TO MAKE VECTORS.
# But I wanted something to quickly extract info from the ROOT formats from HTTSim.
# I don't think the bytestream parts of HTTSim are set up to write out Hough-aware stuff.
# And in fact even the HTTSim output header doesn't really contain everything.
# Eventually we'll want to somehow use Elliot's dataformatting package.

import argparse
import collections
import json
import os
import sys

import ROOT

# Stop ROOT from hijacking sys.argv.
ROOT.PyConfig.IgnoreCommandLineOptions = True


# This is a little weird. Load the Hit Type enum?
import ROOT.HTTHit
ROOT.HitType()

def parseHTTSimRegion(region):
    # Totally hardcoded for now, but in principle we should really have (eta, phi) regions instead of
    # just some flat "region" number.
    eta_region = region
    phi_region = 0

    # I added other phi slices as regions 5-7.
    if region >= 5:
        eta_region = 0
        phi_region = region - 4

    return (eta_region, phi_region)

def read_event(event, sector, deduplicate=False, region=0, road_type=0, second_stage=False):
    road_vectors = []
    track_vectors = []

    header = event.HTTLogicalEventOutputHeader
    ROOT.SetOwnership(header, False)

    eta_region, phi_region = parseHTTSimRegion(region)

    # Get the roads using the pass-by-reference getter.
    roads = ROOT.rvec()
    if not second_stage:
        header.getHTTRoads_1st(roads)

        # TODO THIS IS ALL TRACKS, NOT POST-OR TRACKS.
        tracks = header.getHTTTracks_1st()
    else:
        header.getHTTRoads_2nd(roads)
        tracks = header.getHTTTracks_2nd()

    legal_patterns = set()
    for road in roads:
        if road.getSector() == sector or sector == -1:
            vector = collections.OrderedDict()

            # INTERNAL USE ONLY FOR DEBUGGING.
            # This will not be present in the firmware.
            vector["id"] = road.getPID()

            # For now, store this.
            vector["type"] = road_type

            # The eta and phi regions.
            # HTTRoad doesn't actually have this as metadata? Hmm...
            vector["etaRegion"] = eta_region
            vector["phiRegion"] = phi_region

            # The subregion. Part of the spec, even if not technically used.
            vector["slice"] = road.getSubRegion()

            # The Hough bin numbers. In the real firmware this is all we'll have.
            vector["houghXBin"] = road.getXBin()
            vector["houghYBin"] = road.getYBin()
            # The "true" Hough x/y coordinates, WILL NOT BE AVAILABLE IN FIRMWARE.
            vector["houghX"] = road.getX()
            vector["houghY"] = road.getY()

            # Is this a second stage road? Convenience bit.
            vector["isSecondStage"] = second_stage

            # The hitmask. Store this as a list of 0/1 bits rather than a binary string.
            hitmask = bin(road.getHitLayers())[2:]
            vector["hitmask"] = []
            for layer in range(road.getNLayers()):
                if layer >= len(hitmask):
                    char = 0
                else:
                    char = int(hitmask[-1 - layer])
                if deduplicate and layer % 2 == 0:
                    vector["hitmask"].append(char)

            # GHits; contain layer/r/phi/z.
            vector["hits"] = []
            for layer in range(road.getNLayers()):
                for hit in road.getHits(layer):
                    vector_hit = {}
                    vector_hit["layer"] = hit.getLayer()
                    vector_hit["r"] = hit.getR()
                    vector_hit["phi"] = hit.getGPhi()
                    vector_hit["z"] = hit.getZ()

                    if deduplicate:
                        if vector_hit["layer"] % 2 == 1:
                            continue
                        else:
                            vector_hit["layer"] /= 2

                    if hit.getHitType() == ROOT.wildcard:
                        continue
                    vector["hits"].append(vector_hit)

            legal_patterns.add(road.getPID())

            road_vectors.append(vector)

    for track in tracks:
        if track.getPatternID() in legal_patterns:
            vector = collections.OrderedDict()
            vector["id"] = track.getPatternID()
            vector["chi2ndof"] = track.getChi2ndof()

            vector["hits"] = []
            for hit in track.getHTTHits():
                vector_hit = {}
                vector_hit["layer"] = hit.getLayer()
                vector_hit["r"] = hit.getR()
                vector_hit["phi"] = hit.getGPhi()
                vector_hit["z"] = hit.getZ()

                if deduplicate:
                    if vector_hit["layer"] % 2 == 1:
                        continue
                    else:
                        vector_hit["layer"] /= 2
                vector["hits"].append(vector_hit)

            track_vectors.append(vector)

    del header
    return road_vectors, track_vectors

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="ROOT loghits file to parse.")
    parser.add_argument("-s", "--sector", dest="sector", type=int, default=-1, help="Only select roads from a specific sector.")
    parser.add_argument("-n", "--nevents", dest="nevents", type=int, default=-1, help="Events to parse.")
    parser.add_argument("-i", "--input", dest="input", default="input_roads.json", help="JSON filename for the generated input test vectors.")
    parser.add_argument("-o", "--output", dest="output", default="output_tracks.json", help="JSON filename for the generated output test vectors.")
    parser.add_argument("-d", "--deduplicate", dest="deduplicate", action="store_true", help="Remove duplication of spacepoints.")
    parser.add_argument("-t", "--type", dest="type", default=0, type=int, help="Road type-- this field is meaningless at the moment, default to 0.")
    parser.add_argument("-2", "--second-stage", dest="second_stage", action="store_true", help="Read second stage roads instead of first stage roads.")
    parser.add_argument("-r", "--region", dest="region", default=0, type=int, help="HTTSim region; not stored in HTTRoad apparently.")
    args = parser.parse_args()

    inputs = []
    outputs = []

    rootname = os.path.abspath(os.path.expanduser(args.filename))
    if not os.path.exists(rootname):
        print("No such file or directory: " + rootname)
        sys.exit(1)

    tfile = ROOT.TFile(rootname)
    tree = tfile.Get("HTTLogicalEventTree")
    nevents = tree.GetEntries()
    tfile.Close()

    # This segfaults, wtf. I found something that sounded related that got fixed in 6.24, but this is 6.08.
    # I poked at this a lot and I think there are several issues: the structures HTSim wants to write out are too complex,
    # this version of ROOT is too old, and pyroot is garbage. So we'll do this horribly inefficiently.
    for i in range(nevents):
        if i >= args.nevents and args.nevents >= 0:
            break

        print("Reading event " + str(i))
        tfile = ROOT.TFile(rootname)
        tree = tfile.Get("HTTLogicalEventTree")
        if i >= tree.GetEntries():
            break

        # This is really unfortunate and I don't understand why the header was written this way.
        ROOT.gInterpreter.Declare("""typedef std::vector<HTTRoad*> rvec;""")

        tree.GetEntry(i)
        event_in, event_out = read_event(tree, args.sector, deduplicate=args.deduplicate, region=args.region, road_type=args.type, second_stage=args.second_stage)
        if len(event_in) > 0:
            inputs.append(event_in)
            outputs.append(event_out)

        tfile.Close()

    # Name is misleading here; "input" are the _input test vectors_.
    with open(args.input, 'w') as input_file:
        json.dump(inputs, input_file, indent=4)
    with open(args.output, 'w') as output_file:
        json.dump(outputs, output_file, indent=4)

if __name__ == '__main__':
    main()
