# Coordinate transform and hit implementation

# Hardcode these for now, but we'll change it layer (can just parse the pmap).
# This hardcoding assumes 5 spacepoint layers in the barrel
idealized_radii = {0: 290.516,
                   1: (396.066 + 402.463) / 2.,
                   2: (558.552 + 564.953) / 2.,
                   3: (758.321 + 764.665) / 2,
                   4: (996.384 + 1002.72) / 2.
                  }

class GlobalHit:

    def __init__(self, r, z, phi, layer):
        self.r = r
        self.z = z
        self.phi = phi
        self.layer = layer

        # Default idealized radius to barrel geometry, but we'll change this later.
        self.rprime = idealized_radii[self.layer]

        self.cotangent_method = None

    def __str__(self):
        return "(r={}, z={}, phi={})".format(self.r, self.z, self.phi)

    def __repr__(self):
        return self.__str__()

    def toJson(self, transform=False, qpt=0):
        """ Returns a JSONizable dictionary representation of the hit.
            Optionally, returns a representation of the linearized hit instead."""
        hit = {}
        if not transform:
            hit["layer"] = self.layer
            hit["r"] = self.r
            hit["phi"] = self.phi
            hit["z"] = self.z
        else:
            hit["layer"] = self.layer
            hit["r'"] = self.rprime
            hit["phi'"] = self.getIdealPhi(qpt)
            hit["z'"] = self.getIdealZ(qpt, self.cotangent_method)
        return hit

    def getCotTheta(self, method=None):
        """ Compute cot(theta) using _some method_ (division, r' = r, eta patterns, ML?!..."""
        if method is None or method == '':
            return self.z / self.r
        elif method == 'rprime':
            # This almost certainly isn't good enough but it would be interesting to see how bad it is.
            return self.z / self.rprime
        elif method == 'etapattern':
            raise NotImplementedError("Eta pattern pre-estimate not implemented!")
        elif method == "hls4ml":
            raise NotImplementedError("Neural network guess not implemented!")

    def getRho(self, qpt):
        return 0.0003 * qpt

    def getIdealPhi(self, qpt):
        """ Coordinate transform, convert phi -> phi'."""
        rho = self.getRho(qpt)
        phiprime = self.phi + rho * (self.r - self.rprime) + (self.r * rho)**3 / 6.
        return phiprime

    def getIdealZ(self, qpt, method=None):
        """ Coordinate transform, convert z -> z'. Also sometimes called 'eta coordinate'."""
        rho = self.getRho(qpt)
        cottheta = self.getCotTheta(method=(method if method is not None else self.cotangent_method))
        zprime = self.z - cottheta * (self.r - self.rprime) - (self.r * rho)**2 * self.z / 6.
        return zprime

    def getIdealDeltaPhi(self, qpt, phi0):
        """ Coordinate transform; convert phi -> delta phi' = phi' - phi'_exp.
            Requires estimate of phi0 (Hough x)."""
        phiprime = self.getIdealPhi(qpt)
        rho = self.getRho(qpt)
        expected = phi0 + self.rprime * rho + (self.rprime * rho)**3  / 6.
        return phiprime - expected
