# Actual track fitter, does chi2 multiplication

import numpy

class Track(object):

    def __init__(self, hits):

        # A track is a collection of hits.
        # len(hits) = nlayers, with Nones when there is a missing hit!
        self.hits = hits

        self.hitmask = []

        # The chi2 produced from fitting the track.
        self.chi2 = 0.0
        self.ndof = 0

        # At some point we will need to calculate _some_ track parameters, like d0.
        self.track_params = {}

    def toJson(self):
        """ Returns a JSONizable dictionary representation of the track."""
        track = {}
        track["chi2ndof"] = self.getChi2ndof()
        for param_name, param in self.track_params.items():
            track[param_name] = param
        track["hits"] = []
        for hit in self.hits:
            if hit is not None:
                track["hits"].append(hit.toJson())
        return track

    def getCoordinates(self, houghX, houghY):
        """ Return the fit coordinates, as a numpy array."""
        coords = []
        for hit in self.hits:
            if hit is not None:
                # Since everything is a spacepoint, this is easy!
                coords.append(hit.getIdealPhi(houghY))
                # TODO: make it possible here to pass in different methods for calculating cot(theta).
                # That is currently done when initializing the hit objects instead, which is fine.
                coords.append(hit.getIdealZ(houghY))
            else:
                coords.append(0)
                coords.append(0)

        return numpy.asarray(coords)

    def fit(self, sector, houghX, houghY, fit_params=False):
        """ Fit the track, using the fit constants passed in.
            This is a Sector object from EFTrackFitter.sectors."""
        coords = self.getCoordinates(houghX, houghY)

        # The number of constraints: number of coordinates - 5.
        self.ndof = sector.getNdof()

        # For each constraint, calculate kaverages(i) + kernel(i) * coordinate vector
        for i in range(len(coords) - 5):
            chi = sector.kaverages[i]
            chi += sector.kernel[i].dot(coords)
            self.chi2 += chi**2

        # Fit track parameters, optionally. TODO make it possible to only fit some.
        for key, param_const in sector.param_constants.items():
            param_vector = sector.param_vectors[key]
            self.track_params[key] = (param_const + param_vector.dot(coords))[0]

        # Assign to self.chi2 but also return the value.
        return self.chi2

    def getChi2ndof(self):
        if self.ndof > 0:
            return self.chi2 / self.ndof
        else:
            return -1

class Road(object):

    def __init__(self, hits, constants, houghX, houghY):

        # A road is a collection of hits
        self.hits = hits

        self.constants = constants

        # Various "road ID" fields. in this case the Hough (x, y).
        # NOTE: in reality we'll have integer bin numbers, not houghX, houghY.
        self.houghX = houghX
        self.houghY = houghY
        self.houghXbin = -1
        self.houghYbin = -1

        # Additional road metadata, not required by the fitter.
        self.zslice = 0
        self.etaRegion = 0
        self.phiRegion = 0
        self.secondStage = False
        self.roadType = 0

        # These tracks are really just arrays of hits. Can we do better?
        self.tracks = []

        # This is the best track per road.
        self.bestTrack = None

        # These are the subset of this road's tracks that passed a chi2 cut.
        self.tracksPass = []

        # These are the subset of this road's tracks that passed OR. TODO IMPLEMENT OR.
        self.tracksOR = []

    def fromJson(self, road_dict):
        """ Update road metadata a (JSON) dictionary."""
        self.houghXbin = road_dict["houghXBin"]
        self.houghYbin = road_dict["houghYBin"]
        self.zslice = road_dict["slice"]
        self.etaRegion = road_dict["etaRegion"]
        self.phiRegion = road_dict["phiRegion"]
        self.secondStage = road_dict["isSecondStage"]
        self.roadType = road_dict["type"]

    def toJson(self, tracks=None):
        """ Create JSONizable dictionary for the output tracks.
            If tracks=None, instead write out the road with transformed hits."""
        json_tracks = []
        if tracks is not None:
            for track in tracks:
                json_track = {"type": self.roadType,
                            "etaRegion": self.etaRegion,
                            "phiRegion": self.phiRegion,
                            "slice": self.zslice,
                            "houghXBin": self.houghXbin,
                            "houghYBin": self.houghYbin,
                            "houghX": self.houghX,
                            "houghY": self.houghY,
                            "isSecondStage": self.secondStage}
                json_track.update(track.toJson())
                json_tracks.append(json_track)
            return json_tracks
        else:
            json_road = {"type": self.roadType,
                        "etaRegion": self.etaRegion,
                        "phiRegion": self.phiRegion,
                        "slice": self.zslice,
                        "houghXBin": self.houghXbin,
                        "houghYBin": self.houghYbin,
                        "houghX": self.houghX,
                        "houghY": self.houghY,
                        "isSecondStage": self.secondStage}
            json_road["hits"] = []
            for hitlayer in self.hits:
                # I guess technically there should be a version of this that doesn't do the transform.
                for hit in hitlayer:
                    json_road["hits"].append(hit.toJson(transform=True, qpt=self.houghY))
            return json_road

    def makeTracks(self):
        """ Recursively build tracks using a helper function, returns number of tracks.
            Will do nothing if called a second time, but still return ntracks."""
        if len(self.tracks) == 0:
            candidates = buildTrackCandidates(self.hits, [])
            for candidate in candidates:
                # Create a new track object containing the candidate hits.
                self.tracks.append(Track(candidate))

        return len(self.tracks)

    def fitTracks(self, sector=None):
        """ For each track in this road, perform the linear fit."""

        # Optionally, allow the sector to be overidden.
        if sector == None:
            sector = self.constants

        # If track candidates weren't created, make them.
        if len(self.tracks) == 0:
            self.makeTracks()

        # For each track, fit the track. Keep track of the one with the lowest chi2/DOF.
        bestChi2 = -1
        for track in self.tracks:
            track.fit(sector, self.houghX, self.houghY)
            if track.getChi2ndof() <= bestChi2 or bestChi2 < 0:
                bestChi2 = track.getChi2ndof()
                self.bestTrack = track

# some miscellaneous utility functions.

def buildTrackCandidates(hits, track):
    """ Recursively build a track candidate from a search road."""

    # Base case: there are no more layers, return the track.
    if len(hits) == 0:
        return [track]

    # Recursive case: there is at least one more layer.
    next_layer = hits[0]

    # If there are no hits in the next layer then just use None.
    if len(next_layer) == 0:
        track.append(None)
        return buildTrackCandidates(hits[1:], track)

    # Otherwise; create new tracks for each possible hit. Recursively extend each.
    else:
        new_tracks = []
        for hit in next_layer:
            new_track = track.copy()
            new_track.append(hit)
            new_tracks.extend(buildTrackCandidates(hits[1:], new_track))
        return new_tracks

def computeLayerMask(hits, nlayers, npixel=1, duplicate=False):
    """ Helper function, computes the layer mask with and without duplication.
        This version assumes hits has already been sorted by layer."""
    if duplicate:
        nlayers = (nlayers - npixel) * 2 + npixel
    mask = [False] * nlayers
    for layer, layered_hits in enumerate(hits):
        if duplicate and layer >= npixel:
            layer = 2 * (layer - npixel) + npixel
        status = False
        if len(layered_hits) > 0:
            status = True
        mask[layer] = status
        if duplicate:
            mask[layer + 1] = status
    return mask

def computeLayerMaskFlat(hits, nlayers, npixel=1, duplicate=False):
    """ Helper function, computes the layer mask with and without duplication.
        This function runs over a "flat" hit array."""
    if duplicate:
        nlayers = (nlayers - npixel) * 2 + npixel
    mask = [False] * nlayers
    for hit in hits:
        if hit is not None:
            layer = hit.layer
            if duplicate and layer >= npixel:
                layer = 2 * (layer - npixel) + npixel
            mask[layer] = True
            if duplicate:
                mask[layer + 1] = True
    return mask

def calcQptBin(edges, qpt):
    """ Calculate qpt bin number using bin edges."""
    if qpt < edges[0]:
        return 0
    for i, edge in enumerate(edges):
        if edge > qpt:
            return i - 1
    return len(edges) - 2
