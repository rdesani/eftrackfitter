# Code to manipulate HTTSim map files.
# This _is_ needed for the fit, but also useful for other stuff.

# Right now these functions just move dictionaries and things around.
# This won't really scale though.
# I think there are two distinct "objects" we care about:
# LOGICAL LAYER: index, (idealized) radius, pointer to physical layers
# PHSICAL LAYER: plane map properties

# Then there are REGIONS (and subregions). What's a region?
# A collection of layers with associated eta, phi boundaries.
# So maybe we have two objects: a region (list of logical layers, which are a 1->n map to physlayers)
# and physlayers
# The parsing code should then construct, from the plane map,

import sys

class Region(object):

    def __init__(self, number, nlayers1=9, nlayers2=13):
        self.number = number

        # Underlying layers database, contains dictionaries of physical layer info.
        self.layers = {}

        # Maps of logical layers to specific physical layers.
        self.logical1 = [[] for _ in range(nlayers1)]
        self.logical2 = [[] for _ in range(nlayers2)]

        # The idealized radius of each layer, map of logical layer numbers -> radii.
        self.radii1 = [-1 for _ in range(nlayers1)]
        self.radii2 = [-1 for _ in range(nlayers2)]

        # The deduplicated/spacepointified versions of the above structs.
        self.sp_logical1 = []
        self.sp_logical2 = []
        self.sp_radii1 = []
        self.sp_radii2 = []

    def addPhysLayer(self, isPixel, zone, physLayer, physDisk=-1, side=-1):
        layer_key = (isPixel, zone, physLayer)
        # Instead of an actual class, just use a dictionary to store all layer metadata.
        layer_struct = {"isPixel": isPixel,
                        "zone": zone,
                        "physLayer": physLayer,
                        "physDisk": physDisk,
                        "side": side}

        self.layers[layer_key] = layer_struct

        # If we need this for some reason may as well return it.
        return self.layers[layer_key]

    def updatePhysLayer(self, isPixel, zone, physLayer, eta_min, eta_max, phi_min, phi_max):
        """ Update a given physical layer record with eta and phi ranges for this region."""
        layer_key = (isPixel, zone, physLayer)
        # Create the layer if it's not already there.
        if not layer_key in self.layers:
            self.addPhysLayer(isPixel, zone, physLayer)
        self.layers[layer_key]["etaMin"] = eta_min
        self.layers[layer_key]["etaMax"] = eta_max
        self.layers[layer_key]["phiMin"] = phi_min
        self.layers[layer_key]["phiMax"] = phi_max

        # If we need this for some reason may as well return it.
        return self.layers[layer_key]

    def assignLogicalLayer(self, isPixel, zone, physLayer, radius, index1=-1, index2=-1):
        layer_key = (isPixel, zone, physLayer)
        # Create the layer if it's not already there.
        if not layer_key in self.layers:
            self.addPhysLayer(isPixel, zone, physLayer)

        # Create the logical layer -> physical layer map. Right now this is 1 -> n.
        # Note that while we don't have to use a dictionary here it gives a bit more flexibility.
        stages = [(index1, self.logical1, self.radii1),
                  (index2, self.logical2, self.radii2)]
        for index, logical_map, radius_map in stages:
            if index == -1:
                continue

            # We initialized this to be a list of lists already.
            logical_map[index].append(self.layers[layer_key])

            # Store the radius. This assumes that we have ONE radius per logical layer.
            # Right now those are stored in the pmap per physical layer, so the last one will dominate.
            radius_map[index] = radius

    def deduplicateStrips(self):
        """ Helper function. Combine strip layers and average their radii.
            Assumes that strips and pixels will NEVER be combined into one logical layer."""
        stages = [(self.logical1, self.radii1, self.sp_logical1, self.sp_radii1),
                  (self.logical2, self.radii2, self.sp_logical2, self.sp_radii2)]

        # Do this once for first stage and once for second stage.
        # this approach _does_ hardcode the number of stages but that's totally fine.
        for logical_map, radius_map, sp_map, sp_radii in stages:
            skip_layers = set()
            for layer in (x for x in range(len(logical_map)) if not x in skip_layers):
                physlayers = logical_map[layer]

                # If for some reason there are no physlayers, or if it's a pixel layer, or if it's the final layer
                # (meaning there is no next layer)-- don't deduplicate.
                if len(physlayers) == 0 or physlayers[0]["isPixel"] or layer == len(logical_map) - 1:
                    sp_map.append(physlayers)
                    sp_radii.append(radius_map[layer])
                    continue

                # If we get here, we know we are at a strip layer, and there is an adjacent layer.
                # If those two layers aren't side=0 and side=1, then don't deduplicate.
                nextlayers = logical_map[layer + 1]
                if not (physlayers[0]["side"] == 0 and nextlayers[0]["side"] == 1):
                    sp_map.append(physlayers)
                    sp_radii.append(radius_map[layer])
                    continue

                # If we get here, we know we have two adjacent strip layers with the correct "side" tag.
                sp_radii.append((radius_map[layer] + radius_map[layer + 1]) / 2.)
                sp_map.append([])
                sp_map[-1].extend(physlayers)
                sp_map[-1].extend(nextlayers)

                # Skip the next layer.
                skip_layers.add(layer + 1)

    def getIdealRadii(self, second_stage=False, spacepoints=True):
        """ Retrieve the idealized radius vector for a given stage, with (or without) SP deduplication."""
        if not second_stage:
            return self.sp_radii1 if spacepoints else self.radii1
        else:
            return self.sp_radii2 if spacepoints else self.radii2

    def getLogicalLayers(self, second_stage=False, spacepoints=True):
        """ Retrieves the (isPixel, zone, physLayer) keys for all logical layers."""
        if not second_stage:
            return self.sp_logical1 if spacepoints else self.logical1
        else:
            return self.sp_logical2 if spacepoints else self.logical2

def readPlaneMap(pmap_filename, filter_logical_layers=True):
    """ Read plane map file"""
    metadata = {}
    regions = {}
    with open(pmap_filename) as pmap_file:
        read_header = False
        current_region = 0
        for line in pmap_file:
            line = line.lstrip().rstrip().rstrip('\n').rstrip('\r')
            line = line.replace('\t', ' ')
            if "PMAP_VERSION" in line or "ATLAS-" in line or line == '':
                continue
            if line[0] == '!':
                read_header = True
                continue
            # Process the header. That may, in fact, be all we need?
            if not read_header:
                num, _, tag = line.partition(" ")
                metadata[tag] = int(num)
            elif "region" in line:
                current_region = int(line.split(" ")[1])
                if current_region in regions:
                    print("Error: pmap file malformed!")
                    sys.exit(1)
                # Create a new region object.
                regions[current_region] = Region(current_region, metadata["logical_s1"], metadata["logical_s2"])

            else:
                # Lines are of the following form: "pixel 0    -1    0 plane1 -1    plane2 0".
                # (or similar for strips with an extra field).
                parsed = [x for x in line.split(" ") if x != '']
                is_pixel = False
                zone = int(parsed[1])
                physLayer = int(parsed[3])
                side = -1
                if parsed[0] == "SCT":
                    side = int(parsed[5])
                    first_logical = int(parsed[7])
                    second_logical = int(parsed[9])
                    try:
                        radius = float(parsed[10])
                    except IndexError:
                        radius = -1.0
                else:
                    first_logical = int(parsed[5])
                    second_logical = int(parsed[7])
                    is_pixel = True
                    try:
                        radius = float(parsed[8])
                    except IndexError:
                        radius = -1.0

                # We don't care about anything that's not a logical layer. At least for now.
                # TODO improve this later, support filter_logical_layers = False.
                if filter_logical_layers and (first_logical == -1 and second_logical == -1):
                    continue

                # Now, we know this is a logical layer. We'll update our region accordingly.
                regions[current_region].addPhysLayer(is_pixel, zone, physLayer, physDisk=int(parsed[2]), side=side)

                # Map this layer to a logical layer.
                regions[current_region].assignLogicalLayer(is_pixel, zone, physLayer, radius, first_logical, second_logical)

    return metadata, regions

# TODO these functions still work but they currently don't interact with
# the class I wrote above.

def readRegionMap(rmap_filename, metadata, add_missing=False):
    """ Given a rmap file, returns nphi and the list of towers.
        If add_missing is set, uses plane map metadata to fill in missing layers."""
    ntowers = 0
    nphi = 0
    towers = {}
    current = -1

    last_is_pixel = True
    last_region = 0
    last_physlayer = 0

    with open(rmap_filename) as rmap_file:
        for line in rmap_file:
            line = line.lstrip().rstrip().rstrip('\n').rstrip('\r')
            if line == '':
                continue

            # Handle the first line.
            if "towers" in line:
                header_line = line.split(" ")
                ntowers = int(header_line[1])
                nphi = int(header_line[3])
                continue

            # Handle the start of a new region.
            if line.count(" ") == 0:

                # Make sure we finished things correctly before creating a new region.
                if current != -1 and add_missing:
                    expected = get_expected_layers(metadata, last_is_pixel, last_region)
                    for new_physlayer in range(last_physlayer + 1, expected):
                        new_layer = (last_is_pixel, last_region, new_physlayer, 0, 0, 0, 0, 0, 0)
                        towers[current].append(new_layer)

                current = int(line)
                towers[current] = []
                last_is_pixel = True
                last_region = 0
                last_physlayer = 0
                continue

            # Load the layer. Could do this with classes, but... overkill.
            # TODO we'll need a string conversion...
            layer = tuple(line.split(" "))
            is_pixel = bool(layer[0])
            region = int(layer[1])
            physlayer = int(layer[2])

            # Test to see if we've skipped layers, using the pmap metadata.
            # Are we in the same detector region?
            if add_missing:
                if region == last_region:
                    # If so, physlayer must = last_physlayer + 1.
                    # If it *isn't*, then we need to insert more layers.
                    for new_physlayer in range(last_physlayer + 1, physlayer):
                        # The number of 0s is hardcoded... not sure that's good.
                        new_layer = (is_pixel, region, new_physlayer, 0, 0, 0, 0, 0, 0)
                        towers[current].append(new_layer)

                else:
                    # If we are *not* in the same region, we need to figure out if we finished the last region.
                    # The last region might be in pixels and not strips!
                    expected = get_expected_layers(metadata, last_is_pixel, last_region)
                    for new_physlayer in range(last_physlayer + 1, expected):
                        new_layer = (last_is_pixel, last_region, new_physlayer, 0, 0, 0, 0, 0, 0)
                        towers[current].append(new_layer)

                    # We may also need to insert layers between 0 and physlayer.
                    for new_physlayer in range(0, physlayer):
                        new_layer = (is_pixel, region, new_physlayer, 0, 0, 0, 0, 0, 0)
                        towers[current].append(new_layer)

            # Now that we have possibly inserted layers-- update accordingly.
            towers[current].append(layer)

            # Update the last_ fields accordingly too.
            last_is_pixel = is_pixel
            last_region = region
            last_physlayer = physlayer
        else:
            if add_missing:
                # We can be missing lines at the tail end of the rmap too... this isn't conclusive
                # but hopefully is good enough for now.
                expected = get_expected_layers(metadata, last_is_pixel, last_region)
                for new_physlayer in range(last_physlayer + 1, expected):
                    new_layer = (last_is_pixel, last_region, new_physlayer, 0, 0, 0, 0, 0, 0)
                    towers[current].append(new_layer)

    # What should one do in this case?
    if len(towers) != ntowers:
        print("Malformed RMAP file; expected " + str(towers) + " but file contained " + str(len(towers)) + " towers (regions).")

    return nphi, towers

def getEtaPhiRanges(region, layers):
    """ Given a region from a rmap, extracts the eta/phi ranges for logical layers."""
    # Remove the radius from the logical layers (if it's even there).
    # I wonder if it would be better to have this as a mode on readRegionMap.
    temp_layers = [layer[:-1] for layer in layers]
    eta_bounds = {}
    phi_bounds = {}
    for rmap_layer in region:
        match_layer = (bool(int(rmap_layer[0])), int(rmap_layer[1]), int(rmap_layer[2]))
        if match_layer in temp_layers:
            # Let's discard the "number of modules" information since it's useless anyway.
            logical = temp_layers.index(match_layer)
            eta_bounds[logical] = (int(rmap_layer[6]), int(rmap_layer[7]))
            phi_bounds[logical] = (int(rmap_layer[3]), int(rmap_layer[4]))
    return eta_bounds, phi_bounds

def matchPatternsToRegions(patterns, regions, layers):
    """ Sort eta patterns by subregion."""
    mapped_patterns = {}
    for number in regions.keys():
        mapped_patterns[number] = []

    for pattern in patterns:
        for number, region in regions.items():
            eta_range, phi_range = getEtaPhiRanges(region, layers)
            if isPatternInBounds(pattern, eta_range):
                mapped_patterns[number].append(pattern)

    return mapped_patterns

def isPatternInBounds(pattern, eta_bounds):
    """ For a given eta pattern, is it wholly contained within eta_bounds."""
    contained = True
    for i, module in enumerate(pattern):
        (is_layer, zone, etamod) = module
        (eta_min, eta_max) = eta_bounds[i]
        if etamod < eta_min or etamod > eta_max:
            #print("Layer %d has eta module %d outside range %d, %d" % (i, etamod, eta_min, eta_max))
            contained = False
    return contained

def get_expected_layers(metadata, is_pixel, region):
    """ Helper function for mapping between pmap/rmap layer syntax."""
    metadata_string = ""
    if is_pixel:
        metadata_string += "pixel "
    else:
        metadata_string += "SCT "
    if region == 0:
        metadata_string += "barrel"
    elif region == 1:
        metadata_string += "endcap+"
    elif region == 2:
        metadata_string += "endcap-"
    # Things are zero-indexed!
    return metadata[metadata_string]
