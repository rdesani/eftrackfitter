# This is the "main" fitter implementation that glues the other classes and libraries together.

# For now we assume input data comes via JSON.

import argparse
import json
import os
#import sys

from . import configs
from . import hits
from . import mapping
from . import sectors
from . import tracks

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="JSON input file containing HTTRoads.")

    # Allow this to be set on the command line.
    parser.add_argument("-o", "--output", dest="output", default=os.path.join(os.getcwd(), "output"),
                        help="Output directory, defaults to PWD/output, which will be created if not present.")

    # I dislike this. It would be better to specify a config file in some simple format.
    # Python 3.11 has "tomllib" to parse "toml" files, which are basically .ini files but better (?).
    # Unfortunately we can't assume we'll have python 3.11.
    parser.add_argument("-c", "--config", dest="config", help="Config file for track fitting, contains paths to HTTSim files.")
    parser.add_argument("-s", "--stage", dest="stage", default=1, type=int, help="Which fit stage to run, defaults to 1st stage.")
    parser.add_argument("-n", "--nevents", dest="nevents", default=-1, type=int, help="Number of events to process, -1 defaults to all.")
    parser.add_argument("-z", "--compress", dest="compress", action="store_true", default=False, help="Eliminate unnecessary whitespace in JSON.")

    # This may be unnecessary, but support changing the method for calculating z/r = cot(theta).
    parser.add_argument("-m", "--method", dest="method", default="", help="Method for calculating z/r.", choices=["", "rprime"])

    args = parser.parse_args()

    # Load the configuration file.
    # We _may_ want to allow some settings to be overridden on the CLI in the future.
    # That should be simple enough to do.
    config = configs.loadConfig(args.config)

    # Create output directory if it's not already there.
    output_dir = os.path.abspath(os.path.expanduser(args.output))
    if not os.path.exists(output_dir):
        print("Creating output directory: " + output_dir)
        os.mkdir(output_dir)

    # Based on the configuration file, load the plane map.
    metadata, regions = mapping.readPlaneMap(config["pmap"])
    for num, region in regions.items():
        region.deduplicateStrips()
    print("Loaded logical layer definitions from " + config["pmap"])

    # Okay, this is horribly hardcoded, but how can we figure it out?
    # We probably need to have a mapping in the config file between sector numbers and region files.
    # For now we'll do this.
    npixels = 1 if args.stage == 1 else 5
    second_stage = (args.stage == 2)

    # Based on the configuration file, load sectors and fit constants.
    constants = sectors.createSectors(config["sectors"], config["constants"], npixels=npixels, verbose=True)

    # Load the JSON file and begin parsing roads.
    # Eventually, this should be refactored to not do the JSON parsing directly.
    with open(args.infile) as jsonfile:
        events = json.load(jsonfile)

    # Track some summary statistics as we run.
    nevents = 0
    nroads = 0
    ntracks = 0
    ntracks_pass = 0
    ntracks_or = 0

    # Output tracks for populating JSON.
    all_output_tracks = []
    pass_output_tracks = []
    or_output_tracks = []

    # Output transformed hits for populating JSON.
    output_hits = []

    print("Processing events...")
    for i, event in enumerate(events):
        if i >= args.nevents and args.nevents > 0:
            break

        # Event-level versions of these variables.
        all_tracks = []
        pass_tracks = []
        or_tracks = []
        transformed_roads = []

        # For each road: we will want to do some stuff.
        nevents += 1
        nroads += len(event)
        for road in event:
            # Retrieve the region (i.e. idealized radii, layer info) for this particular road.
            region = regions[road["etaRegion"]] # TODO: assumes that etaregion = htt region.
            radii = region.getIdealRadii(second_stage)

            # Parse the hits for the road. Store them as a list of lists, indexed by layer?
            allhits = [[] for _ in range(len(radii))]
            for hit in road['hits']:
                ghit = hits.GlobalHit(hit['r'], hit['z'], hit['phi'], hit['layer'])
                ghit.cotangent_method = args.method
                # Change the idealized radius to match whatever region this is!
                ghit.rprime = radii[ghit.layer]

                # This is not a valid hit, drop it.
                if ghit.z == 0 and ghit.r == 0 and ghit.phi == 0:
                    continue
                allhits[ghit.layer].append(ghit)

            # TODO CHANGE manually calculate the hit bitmask from the hits that were present.
            # The real data format will have this precomputed for fast referencing.

            # Okay, we have a hitmask. How do we look up the sector?
            # IN PRINCIPLE it's just qpt_bin * hitmask.
            # In practice, it's qpt_bin * sp_hitmask where sp_hitmask[i] = {-1, 0, 1} if (no hit, hit, spacepoint).
            # But in the data format draft I wrote there is not actually any sp vs non-sp information.
            # So the simplest thing to do is to make our sector class convert sp_hitmask -> hitmask and then just map it.
            hitmask = tracks.computeLayerMask(allhits, len(radii), npixels, duplicate=False)
            qptbin = tracks.calcQptBin(config["qptbins"], road['houghY'])

            try:
                sector = constants[(qptbin, tuple(hitmask))]
            except KeyError:
                print("Found road that does not match any sectors with hitmask: " + str(hitmask))
                continue

            # We now have everything we need for a fit:
            # * hit parsing which loads the idealized radii for this particular detector region.
            # * a list of hits, and a hitmask, from which we can create track candidates
            # * a sector which matches the qpt bin and matrix.
            # At this stage, should we create a Road() class, populate it, and have it generate combinations and fit?

            # Create a road object, create track combinations, and fit them.
            groad = tracks.Road(allhits, sector, road['houghX'], road['houghY'])

            # Update metadata on the road object (this is mostly for bookkeeping at the moment).
            groad.fromJson(road)

            # Write out a transformed version of the road.
            transformed_roads.append(groad.toJson(tracks=None))

            # Make tracks and fit them.
            groad.makeTracks()
            groad.fitTracks()
            #print("Processing a road in sector " + str(sector.secID))

            # Post-process the tracks. That can include chi2 cuts and overlap removal.
            # If keepBest = True in the config, *ONLY* keep the best track per road.
            # It's possible this isn't the most elegant way to do this...
            road_tracks = groad.tracks if not config["keepBest"] else [groad.bestTrack]
            ntracks += len(road_tracks)
            for track in road_tracks:
                # We should generate the hitmasks for each track we made, since we'll want to store it in the JSON later.
                track.hitmask = tracks.computeLayerMaskFlat(track.hits, len(radii), npixels, duplicate=False)

                # Check if the track passes a chi2 cut. If so,
                #print(track.getChi2ndof())
                if track.getChi2ndof() < config["chi2cut"] and track.getChi2ndof() >= 0:
                    groad.tracksPass.append(track)
                    ntracks_pass += 1

            # Fill the output dictionaries.
            all_tracks.extend(groad.toJson(road_tracks))
            pass_tracks.extend(groad.toJson(groad.tracksPass))
            or_tracks.extend(groad.toJson(groad.tracksOR))

        if i % 1000 == 0:
            print(" * Processed event " + str(i))
        all_output_tracks.append(all_tracks)
        pass_output_tracks.append(pass_tracks)
        or_output_tracks.append(or_tracks)
        output_hits.append(transformed_roads)
    else:
        print(" * Processed event " + str(i))

    # Create output JSON files.
    # The JSON format is not very efficient; it scales poorly with nevents.
    # For large runs it would be better to use the binary file format (TODO add support for that + converters)
    indent = 4 if not args.compress else None
    with open(os.path.join(output_dir, "output_coords.json"), "w") as output_file:
        json.dump(output_hits, output_file, indent=indent)
        print("Wrote post-coordinate-transform roads to " + output_file.name)
    with open(os.path.join(output_dir, "output_tracks_all.json"), 'w') as output_file:
        json.dump(all_output_tracks, output_file, indent=indent)
        print("Wrote all tracks to " + output_file.name)
    with open(os.path.join(output_dir, "output_tracks_pass.json"), 'w') as output_file:
        json.dump(pass_output_tracks, output_file, indent=indent)
        print("Wrote post-chi2 tracks to " + output_file.name)
    with open(os.path.join(output_dir, "output_tracks_or.json"), 'w') as output_file:
        json.dump(or_output_tracks, output_file, indent=indent)
        print("Wrote post-OR tracks to " + output_file.name)

    # Print summary statistics. These are a little crude compared to HTTSim; we just naively divide by nevents.
    # (instead of actually trying to calculate the mean +/- rms).
    print("Processed {} events:".format(nevents))
    print(" * Input Roads: {}".format(nroads))
    print(" * All Tracks:  {}".format(ntracks))
    print(" * Pass Chi2:   {}".format(ntracks_pass))
    print(" * Pass OR:     {}".format(ntracks_or))
    print("Statistics per Event:")
    print(" * Input Roads: {:.2f}".format(nroads / nevents))
    print(" * All Tracks:  {:.2f}".format(ntracks / nevents))
    print(" * Pass Chi2:   {:.2f}".format(ntracks_pass / nevents))
    print(" * Pass OR:     {:.2f}".format(ntracks_or / nevents))
