# TOML https://toml.io/en/ based configuration / "job options" parser.
# TOML is basically a formal specification for an INI file.
# Config files are _declarative_, not python scripts.

import os
import sys

# Needed to preserve compatibility with python < 3.11 because upstream Python
# refuses to push a "tomllib" package for backwards compatibility.
try:
    import tomllib
except ModuleNotFoundError:
    import tomli as tomllib

def loadConfig(path, strict=True):
    with open(path, 'rb') as toml:
        config = tomllib.load(toml)

    # Do we actually need to do any parsing?
    # I guess we should make sure required fields are set.
    try:
        paths = ["pmap", "constants", "sectors"]
        for path in paths:
            if not os.path.exists(os.path.abspath(config[path])):
                print("Error: no such file or directory: " + config[path])
                if strict:
                    sys.exit(1)

        # these others don't need validation, make sure they're there though.
        other_fields = ["chi2cut", "qptbins", "keepBest"]
        for field in other_fields:
            _ = config[field]
    except KeyError as error:
        print("Error: Missing field: " + str(error))
        if strict:
            sys.exit(1)

    return config
