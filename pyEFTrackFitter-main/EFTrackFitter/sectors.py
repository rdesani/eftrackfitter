# Code for parsing sector/fit constant files from HTTSim and manipulating
# the resulting objects in Python.

import numpy

# This is how they're identified in the fit constant files.
# Technically z0 is not a char but the others are.
param_chars = ["c", "d", "f", "z0", "o"]
# This is a mapping based on the ordering in FitConstantBank.cxx, it might even be right.
param_char_mapping = {"c": "q/pt",
                      "d": "d0",
                      "f": "phi",
                      "z0": "z0",
                      "o": "eta"
                    }

class Sector(object):

    def __init__(self, vector, num_layers):
        self.vector = vector

        # Some fields that may or may not be useful.
        self.bins = vector[1:1 + num_layers]
        self.secID = vector[0]
        self.nevents = vector[-1]

        # Parsed q/pt + bin number.
        self.qptbin = -1
        self.hitmask = []

        # Keep this to be safe.
        self.num_layers = num_layers

        # The fit constant information.
        # TODO add the track parameter constant sets
        self.kernel = []
        self.kaverages = []

        self.param_constants = {}
        self.param_vectors = {}

    def __str__(self):
        string_repr = str(self.vector)
        string_repr = string_repr.replace(",", "")
        return string_repr[1:-1]

    def __repr__(self):
        return str(self)

    def relabel(self, func, verbose=False):
        """ Relabel sector indices by applying a lambda func."""
        new_bins = [func(x) for x in self.bins]

        # Now update self.bins and self.vector.
        self.bins = new_bins
        self.vector[1:1 + self.num_layers] = new_bins

    def addFitConstants(self, constants, ncoords, nconstr):
        """ Convert fit constants from corrgen file into numpy matrices."""
        self.kaverages = numpy.asarray(constants["kaverages"])
        kernel = []
        for i in range(nconstr):
            kernel.append([])
            for j in range(ncoords):
                k = ncoords*i + j
                kernel[i].append(constants["kernel"][k])
        self.kernel = numpy.asarray(kernel)

        # Figure out if we have constants for the other track parameters.
        for key in constants.keys():
            if key[0] == 'C' and key[1:] in param_chars:
                param_char = key[1:]
                param_name = param_char_mapping[param_char]
                self.param_constants[param_name] = constants[key]
                self.param_vectors[param_name] = numpy.asarray(constants["V" + param_char])

    def parseBinNumbers(self, deduplicate=True, npixels=1):
        """ Convert the bin information to a (qpt_bin, hitmask).
            If deduplicate is set, throws out the SP vs non-SP info and deduplicates. Needs npixels."""
        if not deduplicate:
            raise NotImplementedError('Cannot handle merged SPs and non-SPs at the moment.')

        # If ignore_sp is set, we also deduplicate I guess.
        for i, bin_num in enumerate(self.bins):
            # Assume that this won't change from bin number to bin number.
            # TODO check if it does, complain?
            missing = False
            if bin_num >= 0:
                self.qptbin = bin_num // 10
                missing = True

            # Deduplication!
            if i >= npixels and (i - npixels) % 2 == 1:
                continue
            self.hitmask.append(missing)

    def getNdof(self):
        """ Return the number of degrees of freedom from this sector."""
        ndof = 0
        for hit in self.hitmask:
            if hit:
                ndof += 2
        return ndof - 5

def readSectors(filename, verbose=False):
    num_sectors = -1
    num_layers = -1
    sectors = []
    with open(filename) as sectorfile:
        for line in sectorfile:
            # On the first line: figure out how many sectors, layers we have.
            if num_sectors == -1 and num_layers == -1:
                [num_sectors, num_layers] = [int(x) for x in line.split(" ")]

            # On all subsequent lines: read in the sector definitions.
            else:
                vector = [int(x) for x in line.split(" ")]
                sector = Sector(vector, num_layers)
                sectors.append(sector)

    if verbose:
        print("Read " + str(num_sectors) + " sector definitions from " + filename)
    return num_sectors, num_layers, sectors

def readFitConstants(filename, verbose=False):
    """ Read fit constantsts from a gcon file."""
    sectors = {}

    # have we gotten to the meat of the file?
    read_header = False

    # Working metadata that we need as we go.
    label = ""
    index = 0

    with open(filename) as corrgen:
        for line in corrgen:
            # The first "sector" indicates we've read the header.
            # For now, we skip the header and just hardcode the numbers.
            if not read_header:
                if line.lstrip().rstrip() != "sector":
                    continue
                else:
                    read_header = True
                    label = "sector"

            # We have read the header!
            # Check to see if we're looking at a string or not. This is hacky.
            try:
                numerical = float(line.lstrip().rstrip())
                # If the last word read was "sector", this is a sector ID.
                if label == "sector":
                    index = int(numerical)
                    sectors[index] = {}
                # Otherwise, it was an entry in one of the labelled fields.
                else:
                    sectors[index][label].append(numerical)
            except:
                label = line.lstrip().rstrip()
                if label != "sector":
                    sectors[index][label] = []

    # Make these printouts respect some verbose setting.
    if verbose:
        print("Read " + str(len(sectors)) + " fit constants from " + filename)
    return sectors

def makeMatrices(sectors, ncoords, nconstr):
    matrices = {}
    for index, sector in sectors.items():
        kaverages = numpy.asarray(sector["kaverages"])
        kernel = []
        for i in range(nconstr):
            kernel.append([])
            for j in range(ncoords):
                k = ncoords*i + j
                kernel[i].append(sector["kernel"][k])
        np_kernel = numpy.asarray(kernel)
        matrices[index] = (kaverages, np_kernel)

    return matrices

def createSectors(sector_file, constant_file, npixels=1, verbose=False):
    # Load the sector definitions from the sectorsHW file first.
    num_sectors, num_layers, sectors = readSectors(sector_file, verbose=verbose)

    # Now load the fit constants frm the corrgen file.
    constants = readFitConstants(constant_file, verbose=verbose)

    # Convert the fit constants into matrices, using nlayers and npixels.
    # (we need to pull npixels from somewhere!)
    ncoords = num_layers + npixels
    for sector in sectors:
        sector.parseBinNumbers(npixels=npixels)
        sector.addFitConstants(constants[sector.secID], ncoords, ncoords - 5)

    # Reindex the sectors by the bin/layer label array for easy lookup.
    sector_dict = {}
    for sector in sectors:
        sector_dict[(sector.qptbin, tuple(sector.hitmask))] = sector
    return sector_dict
