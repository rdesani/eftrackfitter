#!/usr/bin/env python

# This script transforms a HTTSim sector definition file (sectorsHW)
# between two formats. See relabel_func() below for the actual transformation.
# Run as ./renumber_sectors.py [-i] filename; passing -i will overwrite the
# original file; without -i it will just print the new version to stdout.

# WARNING: This script currently doesn't do much error checking! It assumes it is
# running over a sectorsHW file; the file must have 'sectorsHW' in the name
# but otherwise the format is not checked.

# WARNING WARNING: we don't check to see if a file has already been processed.

import argparse
import os
import shutil
import sys

def relabel_func(sectorbin):
    """ Relabeling function; applied to each entry in each sector.
        This version: converts 100*isSpacepoint + qpt_bin to 10*qpt_bin + is_spacepoint."""
    # Return -1 if there's nothing here!
    if sectorbin == -1:
        return -1

    qpt_bin = sectorbin % 100
    is_spacepoint = sectorbin // 100

    # TODO: add eta pattern, z-slice support.

    return is_spacepoint + 10*qpt_bin

class Sector(object):

    # This class was not strictly

    def __init__(self, vector, num_layers):
        self.vector = vector

        # Some fields that may or may not be useful.
        self.bins = vector[1:1 + num_layers]
        self.secID = vector[0]
        self.nevents = vector[-1]

        # Keep this to be safe.
        self.num_layers = num_layers

    def __str__(self):
        string_repr = str(self.vector)
        string_repr = string_repr.replace(",", "")
        return string_repr[1:-1]

    def __repr__(self):
        return str(self)

    def relabel(self, func, verbose=False):
        """ Relabel sector indices by applying a lambda func."""
        new_bins = [func(x) for x in self.bins]

        # Now update self.bins and self.vector.
        self.bins = new_bins
        self.vector[1:1 + self.num_layers] = new_bins

def parse_sectors(filename, verbose=False):
    num_sectors = -1
    num_layers = -1
    sectors = []
    with open(filename) as sectorfile:
        for line in sectorfile:
            # On the first line: figure out how many sectors, layers we have.
            if num_sectors == -1 and num_layers == -1:
                [num_sectors, num_layers] = [int(x) for x in line.split(" ")]

            # On all subsequent lines: read in the sector definitions.
            else:
                vector = [int(x) for x in line.split(" ")]
                sector = Sector(vector, num_layers)
                sectors.append(sector)

    return num_sectors, num_layers, sectors

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="Sector file to process.")
    parser.add_argument("-i", "--in-place", action="store_true", dest="inplace", help="Edit file in-place; if not specified, just prints out new file.")

    args = parser.parse_args()

    filename = os.path.abspath(os.path.expanduser(args.filename))
    if not os.path.exists(filename):
        print("Error: no such file or directory: " + filename)
        sys.exit(1)

    if "sectorsHW" not in filename:
        print("Error: this doesn't appear to be a sector file. Make sure 'sectorsHW' is in the name.")
        sys.exit(1)

    # Parse file, then relabel sectors.
    num_sectors, num_layers, sectors = parse_sectors(args.filename)
    for sector in sectors:
        sector.relabel(relabel_func)

    # If we passed -i, rewrite the file (e.g. sed -i). Otherwise, just print out the new format.
    if args.inplace:
        shutil.move(filename, filename + ".bak")
        with open(filename, 'w') as newfile:
            newfile.write("%d %d\n" % (num_sectors, num_layers))
            for sector in sectors:
                newfile.write(str(sector) + "\n")
    else:
        print("%d %d" % (num_sectors, num_layers))
        for sector in sectors:
            print(sector)

if __name__ == '__main__':
    main()
