#!/usr/bin/env python

"""
Global coordinate transform for EF Tracking

Test script so we can run the transform without the entire machinery
of HTTSim. Could interface to a similar python script I wrote which
does the fit.

In general I'm wondering if a full lightweight 'pyhttsim' would be
a useful investment; such a thing could potentially be wired to firmware
via cocotb.

What is missing here?
* Different methods to compute z/r (or more accurately, 1/r) : z/r' (won't
work outside the barrel, might be acceptable for now *in* the barrel),
eta pattern pre-estimate of sinh(eta) = z/r; hls4ml nn to do the division.
* I stored q/pt rather than the Hough y bin number
* In general this should run over outputs from the EF tracking test data
utility (i.e. DataFormat.py) rather than my JSON output; then we can enforce
fixed vs floating point here (depending on what we ultimately decide to do),
sizes of bitfields, etc.
"""

import argparse
import json
import os
import sys

from EFTrackFitter import transform

# Plotting only.
try:
    import ROOT

    ROOT.gROOT.LoadMacro("AtlasStyle.C")
    ROOT.gROOT.LoadMacro("AtlasLabels.C")
    ROOT.SetAtlasStyle()
    root_enabled = True
except:
    root_enabled = False

def compareRvsRprime(zcompares):
    canvas = ROOT.TCanvas("canvas", "canvas", 800, 600)
    hist = ROOT.TH1F("r_vs_rprime", "r_vs_rprime", 100, -0.1, 0.1)
    for value in zcompares:
        hist.Fill(value)

    canvas.cd()
    hist.Draw("hist")
    hist.GetXaxis().SetTitle("#it{z'}(cot #it{#theta} = #it{z}/#it{r}) - #it{z'}(cot #it{#theta} = #it{z}/#it{r'}) [mm]")
    hist.GetYaxis().SetTitle("Hits")

    canvas.SaveAs("r_vs_rprime.eps")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="JSON input file containing HTTRoads.")
    parser.add_argument("-o", "--outfile", dest="outfile", default="output_coords.json", help="JSON output file containing transformed coordinates.")
    parser.add_argument("-n", "--nevents", dest="nevents", type=int, default=-1, help="Number of events to parse.")
    parser.add_argument("-m", "--method", dest="method", default="", help="Method for calculating z/r: '', 'rprime', 'etapattern', 'hls4ml.")
    parser.add_argument("-p", "--plot", dest="plot", action="store_true", help="Make some comparison plots, requires ROOT.")
    args = parser.parse_args()

    path = os.path.abspath(args.infile)
    if not os.path.exists(path):
        print("Error: no such file or directory: " + path)
        sys.exit(1)

    with open(path) as jsonfile:
        events = json.load(jsonfile)

    # TODO: factorize this / make it optional.
    zcompares = []

    outevents = []
    for i, event in enumerate(events):
        if i >= args.nevents and args.nevents > 0:
            break
        outroads = []
        for road in event:
            coords = []
            qpt = road['houghY']
            phi0 = road['houghX']
            for hit in road['hits']:
                ghit = transform.GlobalHit(hit['r'], hit['z'], hit['phi'], hit['layer'])
                ghit.cotangent_method = args.method
                phiprime = ghit.getIdealPhi(qpt)
                zprime = ghit.getIdealZ(qpt)
                #print("phi' = " + str(phiprime) + ", z' = " + str(zprime) + ", r' = " + str(ghit.rprime))
                coord = {"layer": hit['layer'], "r'": ghit.rprime, "phi'": phiprime, "z'": zprime}
                coords.append(coord)

                zcompare = ghit.getIdealZ(qpt, method='') - ghit.getIdealZ(qpt, method='rprime')
                zcompares.append(zcompare)

            outjson = {"id": road['id'], 'qpt': qpt, 'input_hits': road['hits'], 'output_coords': coords}
            outroads.append(outjson)
    outevents.append(outroads)

    with open(args.outfile, 'w') as output_file:
        json.dump(outevents, output_file, indent=4)

    if args.plot and root_enabled:
        compareRvsRprime(zcompares)

if __name__ == '__main__':
    main()
