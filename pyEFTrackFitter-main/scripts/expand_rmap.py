#!/usr/bin/env python

# Expands rmap files created by the map maker algorithm using an existing plane map.
# HTTSim currently expects that rmaps and pmaps will all have the same number of lines.
# That means that it isn't possible to use the generated rmaps with the existing
# pmaps (which list *every* layer for every region) without post-processing.

import argparse
import os

def get_expected_layers(metadata, is_pixel, region):
    metadata_string = ""
    if is_pixel:
        metadata_string += "pixel "
    else:
        metadata_string += "SCT "
    if region == 0:
        metadata_string += "barrel"
    elif region == 1:
        metadata_string += "endcap+"
    elif region == 2:
        metadata_string += "endcap-"
    # Things are zero-indexed!
    return metadata[metadata_string]

def str_layer(layer):
    line = ""
    for item in layer:
        line += str(item) + " "
    return line[:-1] + "\n"

def load_pmap(pmap_filename):

    metadata = {}
    regions = {}
    with open(pmap_filename) as pmap_file:
        read_header = False
        current_region = 0
        for line in pmap_file:
            line = line.lstrip().rstrip().rstrip('\n').rstrip('\r')
            if "PMAP_VERSION" in line or "ATLAS-" in line or line == '':
                continue
            if line[0] == '!':
                read_header = True
                continue

            # Process the header. That may, in fact, be all we need?
            if not read_header:
                num, _, tag = line.partition(" ")
                metadata[tag] = int(num)
            elif "region" in line:
                current_region = int(line.split(" ")[1])
                if current_region in regions:
                    print("Error: pmap file malformed!")
                    sys.exit(1)
                regions[current_region] = []
            else:
                # TODO does this need processing?
                regions[current_region].append(line)

    return metadata, regions

def load_rmap(rmap_filename, metadata):
    ntowers = 0
    nphi = 0
    towers = {}
    current = -1

    last_is_pixel = True
    last_region = 0
    last_physlayer = 0

    with open(rmap_filename) as rmap_file:
        for line in rmap_file:
            line = line.lstrip().rstrip().rstrip('\n').rstrip('\r')
            if line == '':
                continue

            # Handle the first line.
            if "towers" in line:
                header_line = line.split(" ")
                ntowers = int(header_line[1])
                nphi = int(header_line[3])
                continue

            # Handle the start of a new region.
            if line.count(" ") == 0:

                # Make sure we finished things correctly before creating a new region.
                if current != -1:
                    expected = get_expected_layers(metadata, last_is_pixel, last_region)
                    for new_physlayer in range(last_physlayer + 1, expected):
                        new_layer = (last_is_pixel, last_region, new_physlayer, 0, 0, 0, 0, 0, 0)
                        towers[current].append(new_layer)

                current = int(line)
                towers[current] = []
                last_is_pixel = True
                last_region = 0
                last_physlayer = 0
                continue

            # Load the layer. Could do this with classes, but... overkill.
            # TODO we'll need a string conversion...
            layer = tuple(line.split(" "))
            is_pixel = int(layer[0])
            region = int(layer[1])
            physlayer = int(layer[2])

            # Test to see if we've skipped layers, using the pmap metadata.
            # Are we in the same detector region?
            if region == last_region:
                # If so, physlayer must = last_physlayer + 1.
                # If it *isn't*, then we need to insert more layers.
                for new_physlayer in range(last_physlayer + 1, physlayer):
                    # The number of 0s is hardcoded... not sure that's good.
                    new_layer = (is_pixel, region, new_physlayer, 0, 0, 0, 0, 0, 0)
                    towers[current].append(new_layer)

            else:
                # If we are *not* in the same region, we need to figure out if we finished the last region.
                # The last region might be in pixels and not strips!
                expected = get_expected_layers(metadata, last_is_pixel, last_region)
                for new_physlayer in range(last_physlayer + 1, expected):
                    new_layer = (last_is_pixel, last_region, new_physlayer, 0, 0, 0, 0, 0, 0)
                    towers[current].append(new_layer)

                # We may also need to insert layers between 0 and physlayer.
                for new_physlayer in range(0, physlayer):
                    new_layer = (is_pixel, region, new_physlayer, 0, 0, 0, 0, 0, 0)
                    towers[current].append(new_layer)

            # Now that we have possibly inserted layers-- update accordingly.
            towers[current].append(layer)

            # Update the last_ fields accordingly too.
            last_is_pixel = is_pixel
            last_region = region
            last_physlayer = physlayer
        else:
            # We can be missing lines at the tail end of the rmap too... this isn't conclusive
            # but hopefully is good enough for now.
            expected = get_expected_layers(metadata, last_is_pixel, last_region)
            for new_physlayer in range(last_physlayer + 1, expected):
                new_layer = (last_is_pixel, last_region, new_physlayer, 0, 0, 0, 0, 0, 0)
                towers[current].append(new_layer)

    return nphi, towers

def write_rmap(output_name, nphi, towers):
    with open(output_name, 'w') as rmap:
        rmap.write("towers " + str(len(towers)) + " phi " + str(nphi) + "\n")
        for index, tower in towers.items():
            rmap.write("\n" + str(index) + "\n")
            for layer in tower:
                rmap.write(str_layer(layer))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("pmap", help="Plane map file.")
    parser.add_argument("rmap", help="Region map file.")
    args = parser.parse_args()

    pmap_filename = os.path.abspath(args.pmap)
    if not os.path.exists(pmap_filename):
        print("Error: no such file or directory: " + pmap_filename)

    rmap_filename = os.path.abspath(args.rmap)
    if not os.path.exists(rmap_filename):
        print("Error: no such file or directory: " + rmap_filename)

    # Process the pmap file to find out how many layers we *should* have.
    metadata, regions = load_pmap(pmap_filename)

    # Now, let's deal with the rmap.
    nphi, towers = load_rmap(rmap_filename, metadata)

    # Now, let's write out a new rmap file.
    new_rmapname = "new_" + os.path.basename(rmap_filename)
    new_rmap = os.path.join(os.getcwd(), new_rmapname)
    write_rmap(new_rmap, nphi, towers)

if __name__ == '__main__':
    main()
