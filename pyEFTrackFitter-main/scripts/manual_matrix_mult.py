#!/usr/bin/env python

# Read corrgen file and manually calculate chi^2.

import argparse
import os
import sys

import numpy

# Spacepoints - 10 points.
vector = numpy.asarray([0.00139148, 18.3652, 0.000716771, 52.9346, -0.000447872,
                        100.201, -0.000445754, 160.952, 0.000430607, 229.693])

# Baseline - 9 points, same track.
#vector = numpy.asarray([0.0023174, 18.3652, 0.00154236, 0.000771789, 0.000184339,
#                        -0.000189529, 0.00114989, 0.00122184, 0.00149125])

# Test another track with lower q/pt.
#vector = numpy.asarray([0.0013647, -18.2004, 0.000565551, 21.6774, 0.000503662,
#                        68.6986, 0.00120973, 127.351, 0.00329092, 192.843])

# This version: I changed my delta phi transform.
vector = numpy.asarray([0.375657, 18.365, 0.362736, 52.9337, 0.343247,
                        100.197, 0.320755, 160.942, 0.294702, 229.668])

# Here's event 0.
#vector = numpy.asarray([0.0252198, 111.93, 0.033758, 128.75, 0.0472248,
#                        165.8, 0.0642446, 207.805, 0.0845003, 259.349])

gconfile = "/home/brosser/htt/HTTSim/bankgen/banks/corrgen_raw_9L_reg0_sp4phi4.gcon"

def readSectors(filename):
    sectors = {}

    # have we gotten to the meat of the file?
    read_header = False

    # Working metadata that we need as we go.
    label = ""
    index = 0

    with open(filename) as corrgen:
        for line in corrgen:
            # The first "sector" indicates we've read the header.
            # For now, we skip the header and just hardcode the numbers.
            if not read_header:
                if line.lstrip().rstrip() != "sector":
                    continue
                else:
                    read_header = True
                    label = "sector"

            # We have read the header!
            # Check to see if we're looking at a string or not. This is hacky.
            try:
                numerical = float(line.lstrip().rstrip())
                # If the last word read was "sector", this is a sector ID.
                if label == "sector":
                    index = int(numerical)
                    sectors[index] = {}
                # Otherwise, it was an entry in one of the labelled fields.
                else:
                    sectors[index][label].append(numerical)
            except:
                label = line.lstrip().rstrip()
                if label != "sector":
                    sectors[index][label] = []

    print("Read " + str(len(sectors)) + " sectors from " + filename)
    return sectors

def makeMatrices(sectors, ncoords, nconstr):
    matrices = {}
    for index, sector in sectors.items():
        kaverages = numpy.asarray(sector["kaverages"])
        kernel = []
        for i in range(nconstr):
            kernel.append([])
            for j in range(ncoords):
                k = ncoords*i + j
                kernel[i].append(sector["kernel"][k])
        np_kernel = numpy.asarray(kernel)
        matrices[index] = (kaverages, np_kernel)

    return matrices

def calculateChi2(matrix, vector, nconstr):
    (kaverages, kernel) = matrix
    chi2 = 0
    for i in range(nconstr):
        chi = kaverages[i]
        chi += kernel[i].dot(vector)
        chi2 += chi**2
    return chi2

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("filename", default=gconfile, help="Constants file to process.")
    parser.add_argument("-n", "--number", type=int, default=15, help="Number of sectors to check from constants file.")
    parser.add_argument("-l", "--layers", type=int, default=9, help="Number of layers; assumes 1 pixel layer.")
    args = parser.parse_args()

    # TODO: read these from the file.
    ncoords = args.layers + 1
    nconstr = ncoords - 5

    print("Running with " + str(args.layers) + " layers, assuming 1 pixel layer = " + str(args.layers + 1) + " coordinates")

    sectors = readSectors(args.filename)
    matrices = makeMatrices(sectors, ncoords, nconstr)

    min_chi2 = -1
    min_index = -1
    for index, matrix in matrices.items():
        chi2 = calculateChi2(matrix, vector, nconstr)
        if index >= args.number:
            continue
        print("Sector " + str(index) + " found chi2/DOF = " + str(chi2/nconstr))
        if chi2 < min_chi2 or min_chi2 < 0:
            min_chi2 = chi2
            min_index = index

    print("Sector " + str(min_index) + " gave smallest chi2/DOF: " + str(min_chi2/nconstr))

if __name__ == '__main__':
    main()
