# pyEFTrackFitter

This is a (work in progress) Python implementation of the
"linear fitting with global coordinates (and spacepoints and
a Hough transform)" track fitting being developed for the ATLAS
phase 2 track trigger upgrade ("EF Tracking").

The main codebase for this is currently part of [HTTSim](https://gitlab.cern.ch/atlas-tdaq-ph2upgrades/atlas-tdaq-htt/tdaq-htt-offline/athena/-/tree/21.9/Trigger/TrigHTT);
which has lots of legacy features from both HTT and FTk. HTTSim
was adapted to simulate various possible EF tracking scenarios
and is still being actively developed. This repository is *not* meant
to replace it but rather to complement it:

* HTTSim can simulate the "full" chain; going from unclustered hits
and ending up with fitted tracks, while pyEFTrackFitter can only
perform the track fit.
* As a consequence, HTTSim can perform both a first and second stage
Hough transform + fit; pyEFTrackFitter can only run either (starting
from first or second stage roads), but not both.
* HTTSim can do hit guessing, recovery fits, etc. and other advanced
things that pyEFTrackFitter currently cannot.
* This is pure-Python, and there are no guarantees about performance.

On the other hand, having a clean/pure-Python version of the fitter
can be useful for other things, such as:

* By focusing on just implementing the fit itself it's a lot simpler
to understand what's going on.
* Debugging; it's much easier to get visibility into what's actually
going on when we make tracks, transform coordinates, and fit them.
* Providing a (software) test bench for development of track fitter
firmware without needing the entire edifice of HTTSim.
* Writing scripts for interacting with HTTSim map and constant files;
this Python package contains some functions for parsing and manipulating
them that could be useful for other tools as well.

### Current Status

At the moment, pyEFTrackFitter supports running the entire fit up
to overlap removal, but with some limitations.

Perhaps the most serious involves spacepoint deduplication. Strip
spacepoints are formed from a pair of strip layers. The assumption
has been made here that *all* strip hits are spacepoints, and that
input roads will *only* have spacepoints (and pixel hits). However,
the geometry and sector definition files from HTTSim use physical
layers. pyEFTrackFitter will merge physical strip layers together
when parsing these files, thereby reducing the number of strip layers
from 2N to N.

We could adapt the code to support mixing spacepoints and single
strip hits if necessary-- it would be relatively easy. But this has
not yet been done.

Everything done here at the moment assumes a software-like environment;
the inputs are floating point values and no particular assumptions about bit sizes,
etc. have been made. In the future, we can adjust these assumptions accordingly.

## Installation

pyEFTrackFitter is distributed as a pure-Python package; there is
no need to have ROOT or Athena installed. To make the installation
simple, a distutils/setuptools-compatible `setup.py` file is provided.
The only nonstandard dependency is `numpy`, but on Python versions
below 3.11, `tomli` is also required. (On Python 3.11 and up, a version
of this module has been included in the standard library).

The simplest way to install the package as follows (replace the KRB5
URL with SSH or HTTPS if you prefer):

```
git clone https://:@gitlab.cern.ch:8443/brosser/pyEFTrackFitter.git
python3 -m pip install --user ./pyEFTrackFitter
```

This will install the `EFTrackFitter` package into your home directory
(`~/local/`), and install the `eftrackfit` script into `~/.local/bin/`.
(You may need to add this directory to your `$PATH`).

Alternatively, you can [first set up a virtualenv](https://docs.python.org/3/library/venv.html).

If you want to install the package in "editable" mode, you can instead
run `pip install --user -e ./pyEFTrackFitter`. Doing so will mean that
changes to the git repo will automatically propagate to the installed
version of the package; so you can simply `git pull` to get the latest
changes without doing anything else.

## Usage

The track fitter can be ran from the command line as follows:

```
eftrackfit -c config.toml roads.json
```

The intention is that *most* configuration should be provided via config file,
which contains things like the chi2 cut to apply as well as the plane map and
sector/fit constant files. These config files are roughly equivalent to algorithm/map/bank
tags in HTTSim. There are only a few additional command line options, which are
described below. The "roads.json" contains a list of roads from HTTSim (or some other
tool!) to fit; the input format is discussed below under "Data Formats".

### Config Files

pyEFTrackFitter uses [TOML](https://toml.io/en/) config files, a very simple minimal
configuration file format commonly used in the Python ecosystem. An example
can be found under `configs/config.toml` and is also included here:

```
# Set plane map file so we know what a "layer" is.
pmap = "/home/bjr/Programming/atlas/git/pyEFTrackFitter/maps/ATLAS-P2-ITK-22-02-00_9L.pmap"

# Set constants (corrgen) and sectors (sectorsHW) files.
constants = "/home/bjr/Programming/atlas/git/pyEFTrackFitter/constants/corrgen_raw_9L_reg0_sponly.gcon"
sectors = "/home/bjr/Programming/atlas/git/pyEFTrackFitter/constants/sectorsHW_raw_9L_reg0_sponly.patt"

# Set a chi2 cut to be applied by the fitter.
chi2cut = 4.8

# qpt bin definitions.
#qptbins = [-1.0, -0.5, 0, 0.5, 1.0]
qptbins = [-1.0, -0.9, -0.75, -0.6, -0.45, -0.3, -0.15, -0.075, 0.075, 0.15, 0.3, 0.45, 0.6, 0.75, 0.9, 1.0]
```

More configuration options can be easily added in the future.

### Command Line Options

There are a few settings that can be set on the command line:

* `-s 1` vs `-s 2`: is this the first stage or the second stage? Among other things,
determines the number of pixel layers, which at the moment is hardcoded to 1 or 5,
respectively.
* `-n nevents`: if set, only run over a subset of the provided events.
* `-m method`: if set to `rprime`, changes the coordinate transform method from
cot(theta) = z/r to cot(theta) = z/r', a rough approximation that works okay
in the barrel and is much faster to calculate. Other coordinate transform methods
can be added in the future.
* `-o output`: the output directory, where any output files are written. Defaults
to "output" in the current working directory.

### Outputs

`eftrackfit` will print some output as it runs, including (very simple) summary
statistics. It will also create additional data files containing:

* Copies of the roads, with hit coordinates transformed to their idealized/linearized
versions (r', z', phi').
* All tracks found by the track fitter, with a chi2/DOF calculated.
* All tracks that passed the configured chi2 cut.
* All tracks that passed the overlap removal algorithm.

## Data Formats

Since pyEFTrackFitter does not perform the (Hough transform) track seeding
step, it needs to receive input data somehow. The intention is to support
(at least) two different formats; at the moment only the human-readable
JSON format is supported.

The input road formats match the [specification](https://indico.cern.ch/event/1263860/contributions/5309485/attachments/2610233/4509574/tdaq_week_global_coordinates.pdf)
presented on backup slide 29 here and [implemented here](https://gitlab.cern.ch/atlas-tdaq-ph2upgrades/atlas-tdaq-eftracking/data-format-tools/httdataformatting/-/blob/master/DataFormat.py#L507). The output track formats, likewise, are assumed to match
the output track format presented in both of these reference as well. Hits
are assumed to contain a layer number and r, phi, and z coordinates. For
the time being no assumptions about precision are made but, in principle,
that could be added.

### JSON Formats

The basic data format used by pyEFTrackFitter is a JSON-based representation.
JSON was chosen because it's both easily human- and machine- readable while
also being hierarchical; it makes editing the properties of a road or hit
very easy. It is, however, obviously not compact and obviously not an
efficient way to store large amounts of test data! The JSON formats are
really intended for debugging a handful of events at a time.

For example, here is a JSON representation of a road as generated by
pyEFTrackFitter:

```
        {
            "id": 17034,
            "type": 0,
            "etaRegion": 0,
            "phiRegion": 0,
            "slice": 0,
            "houghXBin": 94,
            "houghYBin": 77,
            "houghX": 0.3819444477558136,
            "houghY": -0.30092594027519226,
            "isSecondStage": false,
            "hitmask": [
                1,
                1,
                1,
                1,
                0
            ],
            "hits": [
                {
                    "layer": 0,
                    "r": 288.4695739746094,
                    "phi": 0.41069701313972473,
                    "z": 111.15249633789062
                },
                ...
        }
```

`eftrackfit` can run over an input JSON file containing hits, and it will
generate JSON files containing the (transformed) fit coordinates for each
road as well as all tracks, all tracks passing the chi2 cut, and all
tracks that pass overlap removal.

Some JSON files (both inputs and outputs) are collected under the `json`
directory. More input files can be created from HTTSim using the included
`json/json_test_vectors.py` script. This script *does* require pyROOT and
assumes that the HTTSim classes have been loaded following [these instructions](https://gitlab.cern.ch/atlas-tdaq-ph2upgrades/atlas-tdaq-htt/tdaq-htt-offline/athena/-/tree/21.9/Trigger/TrigHTT):

```
json_test_vectors.py [-n nevents] [-s sector] [-r region] [-2] -d httsim.root
```

The `-d` flag enable spacepoint de-duplication (as discussed above). The `-2`
option theoretically toggles between first- and second- stage roads but this hasn't
really been tested. The `-r` option sets the (eta) region number in the output JSON;
HTTRoads don't contain this information but the EFTrack road format we developed
does. The `-n` and `-s` options enable a subset of the events to be selected;
if not passed, all events in all sectors will be read.

The JSON files are created with lots of whitespace for human readability. But
this isn't actually required. For compressability, the whitespace and newlines can
be eliminated; there are probably lots of various ways to do this but the
`json/compress_json.py` script can also do it for you. This doesn't actually "compress"
the files, it just rewrites them without the pretty-printing turned on (saving
a significant amount of space).

### Binary Formats

TODO: support the [EFTrackingTestDataUtility](https://gitlab.cern.ch/atlas-tdaq-ph2upgrades/atlas-tdaq-eftracking/data-format-tools/eftrackingtestdatautility)
binary file formats as well!

## HTTSim Input Files

Some default map files are collected under "maps" and some sector/constant files are
collected under "constants". The included plane map is the spacepoint/9L version
and the constant files were generated assuming a Hough threshold of 7/9 with
spacepoint filtering turned on.

### Plane Map

The idealized radii used in the coordinate transform are read out of the plane map,
as is done in HTTSim. For example, here are the logical layers from region 0:

```
region 0
pixel 0     -1    0	plane1  -1    plane2 0 33.3024
pixel 0     -1    1	plane1  -1    plane2 1 99.1959
pixel 0     -1    2	plane1  -1    plane2 2 159.543
pixel 0     -1    3	plane1  -1    plane2 3 227.638
pixel 0     -1    4	plane1  0     plane2 4 290.516
...
SCT   0     -1    0	stereo 0 plane1 1     plane2 5  396.066
SCT   0     -1    1	stereo 1 plane1 2    plane2 6 402.463
SCT   0     -1    2	stereo 0 plane1 3     plane2 7 558.552
SCT   0     -1    3	stereo 1 plane1 4     plane2 8 564.953
SCT   0     -1    4	stereo 0 plane1 5     plane2 9 758.321
SCT   0     -1    5	stereo 1 plane1 6     plane2 10 764.665
SCT   0     -1    6	stereo 0 plane1 7     plane2 11 996.384
SCT   0     -1    7	stereo 1 plane1 8     plane2 12 1002.72
```

More information on this file format can be found [here](https://gitlab.cern.ch/atlas-tdaq-ph2upgrades/atlas-tdaq-htt/tdaq-htt-offline/athena/-/tree/21.9/Trigger/TrigHTT/TrigHTTMaps),
but the number at the end is the idealized radius of the layer. The "plane1 X"
field indicates that the physical layer is part of logical layer X in the first
stage, and likewise "plane2 X" indicates the same for the second stage.

The region numbers in this file are matched to the "etaRegion" field in the
input road format.

### Sector Definition Files

The first line of the sector definition files indicates how many sectors
there are and how many layers each sector contains. The intention at the moment
is that there will be one of these files per eta region.

```
0 100 101 101 101 101 101 101 101 101 0 83382
...
19 50 51 51 -1 -1 51 51 51 51 0 1863
```

The following lines then identify sectors as (sector number), (sector pattern),
0, (nevents) where "nevents" was the number of events used to form the sector.
The "sector pattern" is subject to change but *at the moment* consists of:

* -1 if there is a missing hit in the layer.
* Otherwise, (is_spacepoint) + 10*qpt_bin.

In the above example, sector 0 corresponds to q/pt bin 10; and the first layer
is a pixel layer while the remaining eight are strip layers from which we construct
spacepoints. Note that spacepoint deduplication means these eight strip layers
will be reduced to four!

Sector 19 corresponds to q/pt bin 5, but there are missing hits in the second pair
of strip layers.

The sector numbers here map into the fit constant files, described in the next
section.

### Constant Files

The fit constant files start with some metadata and then contain a list of the
fit constants for each sector. The "fit constants" here include both the matrix
used for the chi2 calculation as well as the constants needed to calculate the
track parameters.

Sectors begin with `sector \n 0`; the number here matches sector 0 in the
sector definition file. The sector then contains several vectors, constants,
and matrices; each is identified by a string, followed by newline-separated
floating point numbers. The fields in the sector definition file are:

* `Vc`, `Vd`, `Vf`, `Vz0`, `Vo`: vectors of length ncoords (2*npixels + nstrips =
10 for first stage, 18 for second stage). These are used to calculate the
five track parameters.
* `kaverages`: a vector of length nconstraints, where nconstraints is
(ncoords - 5) (since there are 5 track parameters). These are the constant offsets
used to perform the track fit.
* `kernel`: a matrix of size ncoords*nconstraints. This is the matrix used
for the track fit.
* `Cc`, `Cd`, `Cf`, `Cz0`, `Co`: constants used for calculating the five track
parameters, paired with the vectors listed previously.

The track fit is performed by calculating and summing the (ncoordinates)
chi2 components, where each chi component `i` is defined as
`kaverages[i] + kernel[i] * track_coords`. The fit constants could likewise
be calculated as `Cx + Vx * track_coords`.

## Scripts

Some useful scripts have been collected into the `scripts/` directory.
A lot of the parsers in EFTrackFit were originally written for these
scripts; they haven't consistently been updated to use the versions in
the Python package. For now they are just provided as is in case the
functionality is useful:

* `expand_rmap.py`: the plane and region maps generated by the HTTSim
map maker only contain layers that actually have hits. This script takes
an rmap and a plane map and "expands" the rmap to have entries for all
layers that were present in the plane map.
* `manual_matrix_mult.py`: the original version of the Python track fitter;
hardcode the coordinates for a track and then provide a constants file.
The script will try all possible sectors and report the chi2 from each.
* `renumber_sectors.py`: this script applies a lambda function to renumber
the sectors in a sector definition file (by changing e.g. the q/pt bin numbers
from 0,1,2 to 0,10,20 or vice versa).
* `transform.py`: This was an older version of the code to apply the coordinate
transform to input hits.

Eventually more of this functionality should get implemented into the
main package (if it hasn't already).

## Library

At some point some documentation for using pyEFTrackFitter as a library
should be written!

The coordinate transform code mainly lives in `hits.py`, where a `GlobalHit`
class contains methods for applying the transform. The constructor takes the
(r, z, phi, layer) of a hit and class methods return the transformed/idealized
`r'` and `z'` coordinates. Another method can calculate the `dz'`, a possible
alternative transform which has been studied in simulation but is not currently
the default.
